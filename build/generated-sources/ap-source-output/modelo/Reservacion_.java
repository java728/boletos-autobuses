package modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.Corridas;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-06-16T22:03:02")
@StaticMetamodel(Reservacion.class)
public class Reservacion_ { 

    public static volatile SingularAttribute<Reservacion, String> nombrePasajero;
    public static volatile SingularAttribute<Reservacion, Integer> num;
    public static volatile SingularAttribute<Reservacion, Corridas> idcorrida;
    public static volatile SingularAttribute<Reservacion, Integer> idre;

}