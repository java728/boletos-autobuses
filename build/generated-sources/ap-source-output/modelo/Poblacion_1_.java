package modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.Corridas;
import modelo.Municipio;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-06-16T22:03:02")
@StaticMetamodel(Poblacion_1.class)
public class Poblacion_1_ { 

    public static volatile ListAttribute<Poblacion_1, Corridas> corridasList;
    public static volatile ListAttribute<Poblacion_1, Corridas> corridasList1;
    public static volatile SingularAttribute<Poblacion_1, Municipio> idmpo;
    public static volatile SingularAttribute<Poblacion_1, String> nombre;
    public static volatile SingularAttribute<Poblacion_1, Integer> idpob;

}