package modelo;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.Autobus;
import modelo.Poblacion_1;
import modelo.Reservacion;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-06-16T22:03:02")
@StaticMetamodel(Corridas.class)
public class Corridas_ { 

    public static volatile CollectionAttribute<Corridas, Reservacion> reservacionCollection;
    public static volatile SingularAttribute<Corridas, Integer> costo;
    public static volatile SingularAttribute<Corridas, Date> fechaHora;
    public static volatile SingularAttribute<Corridas, Integer> idcorrida;
    public static volatile SingularAttribute<Corridas, Autobus> autobus;
    public static volatile SingularAttribute<Corridas, Poblacion_1> destino;
    public static volatile SingularAttribute<Corridas, Poblacion_1> origen;

}