# Sistema de venta de boletos 



## Indicaciones

El objetivo del sistema es la gestión de venta de boletos de autobúses, proporciona una interfaz gráfica amigable e intuitiva implementada en Java como una aplicación de escritorio. 

## Características

- Registro autobúses
- Registro usuarios
- Registro clientes
- Registro de ventas
- Componente gráfico para selección de asientos de autobus

