/*
 * Clase Corrida
 */
package modelo;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author jhonatanbg
 */
public class Corrida {
    private int idCorrida;
    private Poblacion origen;
    private Poblacion destino;
    private Date fecha_hr;
    private ClaseAutobus claseAutob;
    private int costo;
    private ArrayList<Integer> ocupados;

    public Corrida(int idCorrida, Poblacion origen, Poblacion destino, Date fecha_hr, ClaseAutobus claseAutob, int costo) {
        this.idCorrida = idCorrida;
        this.origen = origen;
        this.destino = destino;
        this.fecha_hr = fecha_hr;
        this.claseAutob = claseAutob;
        this.costo = costo;
        ocupados= new ArrayList<Integer>();
    }

    public int getIdCorrida() {
        return idCorrida;
    }

    public void setIdCorrida(int idCorrida) {
        this.idCorrida = idCorrida;
    }

    public Poblacion getOrigenP() {
        return origen;
    }
    public String getOrigen() {
        return origen.getNombre();
    }

    public void setOrigen(Poblacion origen) {
        this.origen = origen;
    }

    public Poblacion getDestinoP() {
        return destino;
    }
    public String getDestino() {
        return destino.getNombre();
    }

    public void setDestino(Poblacion destino) {
        this.destino = destino;
    }

    public Date getFecha() {
        return fecha_hr;
    }

    public void setFecha(Date fecha_hr) {
        this.fecha_hr = fecha_hr;
    }

    public ClaseAutobus getClaseAutobus() {
        return claseAutob;
    }

    public void setClaseAutob(ClaseAutobus claseAutob) {
        this.claseAutob = claseAutob;
    }

    public int getCosto() {
        return costo;
    }

    public void setCosto(int costo) {
        this.costo = costo;
    }

    public ArrayList<Integer> getOcupados() {
        return ocupados;
    }

    public void setOcupados(ArrayList<Integer> ocupados) {
        this.ocupados = ocupados;
    }
    public int getDisponibles(){
        return claseAutob.getNumAsientos()-ocupados.size();
    }
    
    
}
