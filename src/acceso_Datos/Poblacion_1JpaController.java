/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acceso_Datos;

import acceso_Datos.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import modelo.Municipio;
import modelo.Corridas;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import modelo.Poblacion_1;

/**
 *
 * @author JHONATAN
 */
public class Poblacion_1JpaController implements Serializable {

    public Poblacion_1JpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Poblacion_1 poblacion_1) {
        if (poblacion_1.getCorridasList() == null) {
            poblacion_1.setCorridasList(new ArrayList<Corridas>());
        }
        if (poblacion_1.getCorridasList1() == null) {
            poblacion_1.setCorridasList1(new ArrayList<Corridas>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Municipio idmpo = poblacion_1.getIdmpo();
            if (idmpo != null) {
                idmpo = em.getReference(idmpo.getClass(), idmpo.getIdmpo());
                poblacion_1.setIdmpo(idmpo);
            }
            List<Corridas> attachedCorridasList = new ArrayList<Corridas>();
            for (Corridas corridasListCorridasToAttach : poblacion_1.getCorridasList()) {
                corridasListCorridasToAttach = em.getReference(corridasListCorridasToAttach.getClass(), corridasListCorridasToAttach.getIdcorrida());
                attachedCorridasList.add(corridasListCorridasToAttach);
            }
            poblacion_1.setCorridasList(attachedCorridasList);
            List<Corridas> attachedCorridasList1 = new ArrayList<Corridas>();
            for (Corridas corridasList1CorridasToAttach : poblacion_1.getCorridasList1()) {
                corridasList1CorridasToAttach = em.getReference(corridasList1CorridasToAttach.getClass(), corridasList1CorridasToAttach.getIdcorrida());
                attachedCorridasList1.add(corridasList1CorridasToAttach);
            }
            poblacion_1.setCorridasList1(attachedCorridasList1);
            em.persist(poblacion_1);
            if (idmpo != null) {
                idmpo.getPoblacionList().add(poblacion_1);
                idmpo = em.merge(idmpo);
            }
            for (Corridas corridasListCorridas : poblacion_1.getCorridasList()) {
                Poblacion_1 oldDestinoOfCorridasListCorridas = corridasListCorridas.getDestino();
                corridasListCorridas.setDestino(poblacion_1);
                corridasListCorridas = em.merge(corridasListCorridas);
                if (oldDestinoOfCorridasListCorridas != null) {
                    oldDestinoOfCorridasListCorridas.getCorridasList().remove(corridasListCorridas);
                    oldDestinoOfCorridasListCorridas = em.merge(oldDestinoOfCorridasListCorridas);
                }
            }
            for (Corridas corridasList1Corridas : poblacion_1.getCorridasList1()) {
                Poblacion_1 oldOrigenOfCorridasList1Corridas = corridasList1Corridas.getOrigen();
                corridasList1Corridas.setOrigen(poblacion_1);
                corridasList1Corridas = em.merge(corridasList1Corridas);
                if (oldOrigenOfCorridasList1Corridas != null) {
                    oldOrigenOfCorridasList1Corridas.getCorridasList1().remove(corridasList1Corridas);
                    oldOrigenOfCorridasList1Corridas = em.merge(oldOrigenOfCorridasList1Corridas);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Poblacion_1 poblacion_1) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Poblacion_1 persistentPoblacion_1 = em.find(Poblacion_1.class, poblacion_1.getIdpob());
            Municipio idmpoOld = persistentPoblacion_1.getIdmpo();
            Municipio idmpoNew = poblacion_1.getIdmpo();
            List<Corridas> corridasListOld = persistentPoblacion_1.getCorridasList();
            List<Corridas> corridasListNew = poblacion_1.getCorridasList();
            List<Corridas> corridasList1Old = persistentPoblacion_1.getCorridasList1();
            List<Corridas> corridasList1New = poblacion_1.getCorridasList1();
            if (idmpoNew != null) {
                idmpoNew = em.getReference(idmpoNew.getClass(), idmpoNew.getIdmpo());
                poblacion_1.setIdmpo(idmpoNew);
            }
            List<Corridas> attachedCorridasListNew = new ArrayList<Corridas>();
            for (Corridas corridasListNewCorridasToAttach : corridasListNew) {
                corridasListNewCorridasToAttach = em.getReference(corridasListNewCorridasToAttach.getClass(), corridasListNewCorridasToAttach.getIdcorrida());
                attachedCorridasListNew.add(corridasListNewCorridasToAttach);
            }
            corridasListNew = attachedCorridasListNew;
            poblacion_1.setCorridasList(corridasListNew);
            List<Corridas> attachedCorridasList1New = new ArrayList<Corridas>();
            for (Corridas corridasList1NewCorridasToAttach : corridasList1New) {
                corridasList1NewCorridasToAttach = em.getReference(corridasList1NewCorridasToAttach.getClass(), corridasList1NewCorridasToAttach.getIdcorrida());
                attachedCorridasList1New.add(corridasList1NewCorridasToAttach);
            }
            corridasList1New = attachedCorridasList1New;
            poblacion_1.setCorridasList1(corridasList1New);
            poblacion_1 = em.merge(poblacion_1);
            if (idmpoOld != null && !idmpoOld.equals(idmpoNew)) {
                idmpoOld.getPoblacionList().remove(poblacion_1);
                idmpoOld = em.merge(idmpoOld);
            }
            if (idmpoNew != null && !idmpoNew.equals(idmpoOld)) {
                idmpoNew.getPoblacionList().add(poblacion_1);
                idmpoNew = em.merge(idmpoNew);
            }
            for (Corridas corridasListOldCorridas : corridasListOld) {
                if (!corridasListNew.contains(corridasListOldCorridas)) {
                    corridasListOldCorridas.setDestino(null);
                    corridasListOldCorridas = em.merge(corridasListOldCorridas);
                }
            }
            for (Corridas corridasListNewCorridas : corridasListNew) {
                if (!corridasListOld.contains(corridasListNewCorridas)) {
                    Poblacion_1 oldDestinoOfCorridasListNewCorridas = corridasListNewCorridas.getDestino();
                    corridasListNewCorridas.setDestino(poblacion_1);
                    corridasListNewCorridas = em.merge(corridasListNewCorridas);
                    if (oldDestinoOfCorridasListNewCorridas != null && !oldDestinoOfCorridasListNewCorridas.equals(poblacion_1)) {
                        oldDestinoOfCorridasListNewCorridas.getCorridasList().remove(corridasListNewCorridas);
                        oldDestinoOfCorridasListNewCorridas = em.merge(oldDestinoOfCorridasListNewCorridas);
                    }
                }
            }
            for (Corridas corridasList1OldCorridas : corridasList1Old) {
                if (!corridasList1New.contains(corridasList1OldCorridas)) {
                    corridasList1OldCorridas.setOrigen(null);
                    corridasList1OldCorridas = em.merge(corridasList1OldCorridas);
                }
            }
            for (Corridas corridasList1NewCorridas : corridasList1New) {
                if (!corridasList1Old.contains(corridasList1NewCorridas)) {
                    Poblacion_1 oldOrigenOfCorridasList1NewCorridas = corridasList1NewCorridas.getOrigen();
                    corridasList1NewCorridas.setOrigen(poblacion_1);
                    corridasList1NewCorridas = em.merge(corridasList1NewCorridas);
                    if (oldOrigenOfCorridasList1NewCorridas != null && !oldOrigenOfCorridasList1NewCorridas.equals(poblacion_1)) {
                        oldOrigenOfCorridasList1NewCorridas.getCorridasList1().remove(corridasList1NewCorridas);
                        oldOrigenOfCorridasList1NewCorridas = em.merge(oldOrigenOfCorridasList1NewCorridas);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = poblacion_1.getIdpob();
                if (findPoblacion_1(id) == null) {
                    throw new NonexistentEntityException("The poblacion_1 with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Poblacion_1 poblacion_1;
            try {
                poblacion_1 = em.getReference(Poblacion_1.class, id);
                poblacion_1.getIdpob();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The poblacion_1 with id " + id + " no longer exists.", enfe);
            }
            Municipio idmpo = poblacion_1.getIdmpo();
            if (idmpo != null) {
                idmpo.getPoblacionList().remove(poblacion_1);
                idmpo = em.merge(idmpo);
            }
            List<Corridas> corridasList = poblacion_1.getCorridasList();
            for (Corridas corridasListCorridas : corridasList) {
                corridasListCorridas.setDestino(null);
                corridasListCorridas = em.merge(corridasListCorridas);
            }
            List<Corridas> corridasList1 = poblacion_1.getCorridasList1();
            for (Corridas corridasList1Corridas : corridasList1) {
                corridasList1Corridas.setOrigen(null);
                corridasList1Corridas = em.merge(corridasList1Corridas);
            }
            em.remove(poblacion_1);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Poblacion_1> findPoblacion_1Entities() {
        return findPoblacion_1Entities(true, -1, -1);
    }

    public List<Poblacion_1> findPoblacion_1Entities(int maxResults, int firstResult) {
        return findPoblacion_1Entities(false, maxResults, firstResult);
    }

    private List<Poblacion_1> findPoblacion_1Entities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Poblacion_1.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Poblacion_1 findPoblacion_1(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Poblacion_1.class, id);
        } finally {
            em.close();
        }
    }

    public int getPoblacion_1Count() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Poblacion_1> rt = cq.from(Poblacion_1.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
