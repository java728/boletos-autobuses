/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acceso_Datos;

import acceso_Datos.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import modelo.Poblacion_1;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import modelo.Municipio;

/**
 *
 * @author JHONATAN
 */
public class MunicipioJpaController implements Serializable {

    public MunicipioJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Municipio municipio) {
        if (municipio.getPoblacionList() == null) {
            municipio.setPoblacionList(new ArrayList<Poblacion_1>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Poblacion_1> attachedPoblacionList = new ArrayList<Poblacion_1>();
            for (Poblacion_1 poblacionListPoblacion_1ToAttach : municipio.getPoblacionList()) {
                poblacionListPoblacion_1ToAttach = em.getReference(poblacionListPoblacion_1ToAttach.getClass(), poblacionListPoblacion_1ToAttach.getIdpob());
                attachedPoblacionList.add(poblacionListPoblacion_1ToAttach);
            }
            municipio.setPoblacionList(attachedPoblacionList);
            em.persist(municipio);
            for (Poblacion_1 poblacionListPoblacion_1 : municipio.getPoblacionList()) {
                Municipio oldIdmpoOfPoblacionListPoblacion_1 = poblacionListPoblacion_1.getIdmpo();
                poblacionListPoblacion_1.setIdmpo(municipio);
                poblacionListPoblacion_1 = em.merge(poblacionListPoblacion_1);
                if (oldIdmpoOfPoblacionListPoblacion_1 != null) {
                    oldIdmpoOfPoblacionListPoblacion_1.getPoblacionList().remove(poblacionListPoblacion_1);
                    oldIdmpoOfPoblacionListPoblacion_1 = em.merge(oldIdmpoOfPoblacionListPoblacion_1);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Municipio municipio) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Municipio persistentMunicipio = em.find(Municipio.class, municipio.getIdmpo());
            List<Poblacion_1> poblacionListOld = persistentMunicipio.getPoblacionList();
            List<Poblacion_1> poblacionListNew = municipio.getPoblacionList();
            List<Poblacion_1> attachedPoblacionListNew = new ArrayList<Poblacion_1>();
            for (Poblacion_1 poblacionListNewPoblacion_1ToAttach : poblacionListNew) {
                poblacionListNewPoblacion_1ToAttach = em.getReference(poblacionListNewPoblacion_1ToAttach.getClass(), poblacionListNewPoblacion_1ToAttach.getIdpob());
                attachedPoblacionListNew.add(poblacionListNewPoblacion_1ToAttach);
            }
            poblacionListNew = attachedPoblacionListNew;
            municipio.setPoblacionList(poblacionListNew);
            municipio = em.merge(municipio);
            for (Poblacion_1 poblacionListOldPoblacion_1 : poblacionListOld) {
                if (!poblacionListNew.contains(poblacionListOldPoblacion_1)) {
                    poblacionListOldPoblacion_1.setIdmpo(null);
                    poblacionListOldPoblacion_1 = em.merge(poblacionListOldPoblacion_1);
                }
            }
            for (Poblacion_1 poblacionListNewPoblacion_1 : poblacionListNew) {
                if (!poblacionListOld.contains(poblacionListNewPoblacion_1)) {
                    Municipio oldIdmpoOfPoblacionListNewPoblacion_1 = poblacionListNewPoblacion_1.getIdmpo();
                    poblacionListNewPoblacion_1.setIdmpo(municipio);
                    poblacionListNewPoblacion_1 = em.merge(poblacionListNewPoblacion_1);
                    if (oldIdmpoOfPoblacionListNewPoblacion_1 != null && !oldIdmpoOfPoblacionListNewPoblacion_1.equals(municipio)) {
                        oldIdmpoOfPoblacionListNewPoblacion_1.getPoblacionList().remove(poblacionListNewPoblacion_1);
                        oldIdmpoOfPoblacionListNewPoblacion_1 = em.merge(oldIdmpoOfPoblacionListNewPoblacion_1);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = municipio.getIdmpo();
                if (findMunicipio(id) == null) {
                    throw new NonexistentEntityException("The municipio with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Municipio municipio;
            try {
                municipio = em.getReference(Municipio.class, id);
                municipio.getIdmpo();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The municipio with id " + id + " no longer exists.", enfe);
            }
            List<Poblacion_1> poblacionList = municipio.getPoblacionList();
            for (Poblacion_1 poblacionListPoblacion_1 : poblacionList) {
                poblacionListPoblacion_1.setIdmpo(null);
                poblacionListPoblacion_1 = em.merge(poblacionListPoblacion_1);
            }
            em.remove(municipio);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Municipio> findMunicipioEntities() {
        return findMunicipioEntities(true, -1, -1);
    }

    public List<Municipio> findMunicipioEntities(int maxResults, int firstResult) {
        return findMunicipioEntities(false, maxResults, firstResult);
    }

    private List<Municipio> findMunicipioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Municipio.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Municipio findMunicipio(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Municipio.class, id);
        } finally {
            em.close();
        }
    }

    public int getMunicipioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Municipio> rt = cq.from(Municipio.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
