/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acceso_Datos;

import acceso_Datos.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import modelo.Autobus;
import modelo.Poblacion_1;
import modelo.Reservacion;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import modelo.Corridas;

/**
 *
 * @author JHONATAN
 */
public class CorridasJpaController implements Serializable {

    public CorridasJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Corridas corridas) {
        if (corridas.getReservacionCollection() == null) {
            corridas.setReservacionCollection(new ArrayList<Reservacion>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Autobus autobus = corridas.getAutobus();
            if (autobus != null) {
                autobus = em.getReference(autobus.getClass(), autobus.getIdautobus());
                corridas.setAutobus(autobus);
            }
            Poblacion_1 destino = corridas.getDestino();
            if (destino != null) {
                destino = em.getReference(destino.getClass(), destino.getIdpob());
                corridas.setDestino(destino);
            }
            Poblacion_1 origen = corridas.getOrigen();
            if (origen != null) {
                origen = em.getReference(origen.getClass(), origen.getIdpob());
                corridas.setOrigen(origen);
            }
            Collection<Reservacion> attachedReservacionCollection = new ArrayList<Reservacion>();
            for (Reservacion reservacionCollectionReservacionToAttach : corridas.getReservacionCollection()) {
                reservacionCollectionReservacionToAttach = em.getReference(reservacionCollectionReservacionToAttach.getClass(), reservacionCollectionReservacionToAttach.getIdre());
                attachedReservacionCollection.add(reservacionCollectionReservacionToAttach);
            }
            corridas.setReservacionCollection(attachedReservacionCollection);
            em.persist(corridas);
            if (autobus != null) {
                autobus.getCorridasList().add(corridas);
                autobus = em.merge(autobus);
            }
            if (destino != null) {
                destino.getCorridasList().add(corridas);
                destino = em.merge(destino);
            }
            if (origen != null) {
                origen.getCorridasList().add(corridas);
                origen = em.merge(origen);
            }
            for (Reservacion reservacionCollectionReservacion : corridas.getReservacionCollection()) {
                Corridas oldIdcorridaOfReservacionCollectionReservacion = reservacionCollectionReservacion.getIdcorrida();
                reservacionCollectionReservacion.setIdcorrida(corridas);
                reservacionCollectionReservacion = em.merge(reservacionCollectionReservacion);
                if (oldIdcorridaOfReservacionCollectionReservacion != null) {
                    oldIdcorridaOfReservacionCollectionReservacion.getReservacionCollection().remove(reservacionCollectionReservacion);
                    oldIdcorridaOfReservacionCollectionReservacion = em.merge(oldIdcorridaOfReservacionCollectionReservacion);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Corridas corridas) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Corridas persistentCorridas = em.find(Corridas.class, corridas.getIdcorrida());
            Autobus autobusOld = persistentCorridas.getAutobus();
            Autobus autobusNew = corridas.getAutobus();
            Poblacion_1 destinoOld = persistentCorridas.getDestino();
            Poblacion_1 destinoNew = corridas.getDestino();
            Poblacion_1 origenOld = persistentCorridas.getOrigen();
            Poblacion_1 origenNew = corridas.getOrigen();
            Collection<Reservacion> reservacionCollectionOld = persistentCorridas.getReservacionCollection();
            Collection<Reservacion> reservacionCollectionNew = corridas.getReservacionCollection();
            if (autobusNew != null) {
                autobusNew = em.getReference(autobusNew.getClass(), autobusNew.getIdautobus());
                corridas.setAutobus(autobusNew);
            }
            if (destinoNew != null) {
                destinoNew = em.getReference(destinoNew.getClass(), destinoNew.getIdpob());
                corridas.setDestino(destinoNew);
            }
            if (origenNew != null) {
                origenNew = em.getReference(origenNew.getClass(), origenNew.getIdpob());
                corridas.setOrigen(origenNew);
            }
            Collection<Reservacion> attachedReservacionCollectionNew = new ArrayList<Reservacion>();
            for (Reservacion reservacionCollectionNewReservacionToAttach : reservacionCollectionNew) {
                reservacionCollectionNewReservacionToAttach = em.getReference(reservacionCollectionNewReservacionToAttach.getClass(), reservacionCollectionNewReservacionToAttach.getIdre());
                attachedReservacionCollectionNew.add(reservacionCollectionNewReservacionToAttach);
            }
            reservacionCollectionNew = attachedReservacionCollectionNew;
            corridas.setReservacionCollection(reservacionCollectionNew);
            corridas = em.merge(corridas);
            if (autobusOld != null && !autobusOld.equals(autobusNew)) {
                autobusOld.getCorridasList().remove(corridas);
                autobusOld = em.merge(autobusOld);
            }
            if (autobusNew != null && !autobusNew.equals(autobusOld)) {
                autobusNew.getCorridasList().add(corridas);
                autobusNew = em.merge(autobusNew);
            }
            if (destinoOld != null && !destinoOld.equals(destinoNew)) {
                destinoOld.getCorridasList().remove(corridas);
                destinoOld = em.merge(destinoOld);
            }
            if (destinoNew != null && !destinoNew.equals(destinoOld)) {
                destinoNew.getCorridasList().add(corridas);
                destinoNew = em.merge(destinoNew);
            }
            if (origenOld != null && !origenOld.equals(origenNew)) {
                origenOld.getCorridasList().remove(corridas);
                origenOld = em.merge(origenOld);
            }
            if (origenNew != null && !origenNew.equals(origenOld)) {
                origenNew.getCorridasList().add(corridas);
                origenNew = em.merge(origenNew);
            }
            for (Reservacion reservacionCollectionOldReservacion : reservacionCollectionOld) {
                if (!reservacionCollectionNew.contains(reservacionCollectionOldReservacion)) {
                    reservacionCollectionOldReservacion.setIdcorrida(null);
                    reservacionCollectionOldReservacion = em.merge(reservacionCollectionOldReservacion);
                }
            }
            for (Reservacion reservacionCollectionNewReservacion : reservacionCollectionNew) {
                if (!reservacionCollectionOld.contains(reservacionCollectionNewReservacion)) {
                    Corridas oldIdcorridaOfReservacionCollectionNewReservacion = reservacionCollectionNewReservacion.getIdcorrida();
                    reservacionCollectionNewReservacion.setIdcorrida(corridas);
                    reservacionCollectionNewReservacion = em.merge(reservacionCollectionNewReservacion);
                    if (oldIdcorridaOfReservacionCollectionNewReservacion != null && !oldIdcorridaOfReservacionCollectionNewReservacion.equals(corridas)) {
                        oldIdcorridaOfReservacionCollectionNewReservacion.getReservacionCollection().remove(reservacionCollectionNewReservacion);
                        oldIdcorridaOfReservacionCollectionNewReservacion = em.merge(oldIdcorridaOfReservacionCollectionNewReservacion);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = corridas.getIdcorrida();
                if (findCorridas(id) == null) {
                    throw new NonexistentEntityException("The corridas with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Corridas corridas;
            try {
                corridas = em.getReference(Corridas.class, id);
                corridas.getIdcorrida();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The corridas with id " + id + " no longer exists.", enfe);
            }
            Autobus autobus = corridas.getAutobus();
            if (autobus != null) {
                autobus.getCorridasList().remove(corridas);
                autobus = em.merge(autobus);
            }
            Poblacion_1 destino = corridas.getDestino();
            if (destino != null) {
                destino.getCorridasList().remove(corridas);
                destino = em.merge(destino);
            }
            Poblacion_1 origen = corridas.getOrigen();
            if (origen != null) {
                origen.getCorridasList().remove(corridas);
                origen = em.merge(origen);
            }
            Collection<Reservacion> reservacionCollection = corridas.getReservacionCollection();
            for (Reservacion reservacionCollectionReservacion : reservacionCollection) {
                reservacionCollectionReservacion.setIdcorrida(null);
                reservacionCollectionReservacion = em.merge(reservacionCollectionReservacion);
            }
            em.remove(corridas);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Corridas> findCorridasEntities() {
        return findCorridasEntities(true, -1, -1);
    }

    public List<Corridas> findCorridasEntities(int maxResults, int firstResult) {
        return findCorridasEntities(false, maxResults, firstResult);
    }

    private List<Corridas> findCorridasEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Corridas.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Corridas findCorridas(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Corridas.class, id);
        } finally {
            em.close();
        }
    }

    public int getCorridasCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Corridas> rt = cq.from(Corridas.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
